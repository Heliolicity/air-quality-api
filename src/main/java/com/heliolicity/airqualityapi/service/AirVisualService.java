package com.heliolicity.airqualityapi.service;

public interface AirVisualService {

    void refreshCountries();

    void refreshStates();

    void refreshCities();

}
