package com.heliolicity.airqualityapi.service;

public interface PingMessageService {

    void sendMessage(String message);

}
