package com.heliolicity.airqualityapi.service;

import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;

import java.util.List;

public interface GeoLocationService {

    List<Country> getAllCountries();

    List<State> getAllStates();

    List<State> getAllStatesForCountry(Country country);

    List<City> getAllCities();

    List<City> getAllCitiesForState(State state);

    List<City> getAllCitiesForCountry(Country country);

    void upsertCountries(List<Country> countries);

    void upsertStates(List<State> states);

    void upsertCities(List<City> cities);

}
