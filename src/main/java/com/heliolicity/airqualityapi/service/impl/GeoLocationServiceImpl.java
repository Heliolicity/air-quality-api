package com.heliolicity.airqualityapi.service.impl;

import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;
import com.heliolicity.airqualityapi.exception.InsertException;
import com.heliolicity.airqualityapi.exception.UpdateException;
import com.heliolicity.airqualityapi.mapper.GeoLocationMapper;
import com.heliolicity.airqualityapi.service.GeoLocationService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Slf4j
@Service
public class GeoLocationServiceImpl implements GeoLocationService {

    //TODO: Add an aspect logger to log service calls

    private GeoLocationMapper geoLocationMapper;

    @Autowired
    public GeoLocationServiceImpl(GeoLocationMapper geoLocationMapper) {
        this.geoLocationMapper = geoLocationMapper;
    }

    @Override
    public List<Country> getAllCountries() {
        log.info("In service");
        return geoLocationMapper.findAllCountries();
    }

    @Override
    public List<State> getAllStates() {
        log.info("In service");
        return geoLocationMapper.findAllStates();
    }

    @Override
    public List<State> getAllStatesForCountry(Country country) {
        log.info("In service");
        if (!isCountryValid(country)) {
            return Collections.emptyList();
        }
        return geoLocationMapper.findAllStatesForCountry(country);
    }

    @Override
    public List<City> getAllCities() {
        log.info("In service");
        return geoLocationMapper.findAllCities();
    }

    @Override
    public List<City> getAllCitiesForState(State state) {
        log.info("In service");
        if (!isStateValid(state)) {
            return Collections.emptyList();
        }
        return geoLocationMapper.findAllCitiesForState(state);
    }

    @Override
    public List<City> getAllCitiesForCountry(Country country) {
        log.info("In service");
        if (!isCountryValid(country)) {
            return Collections.emptyList();
        }
        return geoLocationMapper.findAllCitiesForCountry(country);
    }

    @Override
    @Transactional
    public void upsertCountries(List<Country> countries) {
        log.info("Upserting list of Countries");
        if (hasElements(countries)) {
            //geoLocationMapper.upsertCountries(countries);

            for (Country country : countries) {
                Optional<Country> optionalCountry = geoLocationMapper.findCountryByName(country.getName());

                if (optionalCountry.isPresent()) {
                    Country savedCountry = optionalCountry.get();
                    country.setId(savedCountry.getId());
                    updateCountry(country);
                }
                else {
                    insertCountry(country);
                }
            }
        }
    }

    @Override
    @Transactional
    public void upsertStates(List<State> states) {
        log.info("Upserting list of States");
        if (hasElements(states)) {
            //geoLocationMapper.upsertStates(states);

            for (State state : states) {
                Optional<State> optionalState = geoLocationMapper.findStateByName(state.getName());

                if (optionalState.isPresent()) {
                    State savedState = optionalState.get();
                    state.setId(savedState.getId());
                    state.setCountryId(savedState.getCountryId());
                    updateState(state);
                }
                else {
                    insertState(state);
                }
            }
        }
    }

    @Override
    @Transactional
    public void upsertCities(List<City> cities) {
        log.info("Upserting list of Cities");
        if (hasElements(cities)) {
            log.info("Upserting list of Cities");

            //geoLocationMapper.upsertCities(cities);
            for (City city : cities) {
                Optional<City> optionalCity = geoLocationMapper.findCityByName(city.getName());

                if (optionalCity.isPresent()) {
                    City savedCity = optionalCity.get();
                    city.setId(savedCity.getId());
                    city.setStateId(savedCity.getStateId());
                    updateCity(city);
                }
                else {
                    insertCity(city);
                }
            }
        }
    }

    private boolean isCountryValid(Country country) {
        if (!getCountryNames().contains(country.getName().toUpperCase())) {
            log.warn(format("Country %s does not exist", country.getName()));
            return false;
        }
        return true;
    }

    private boolean isStateValid(State state) {
        if (!getStateNames().contains(state.getName().toUpperCase()) ||
                !getCountryNames().contains(state.getCountryName().toUpperCase())) {
            log.warn(format("State %s for Country %s does not exist", state.getName(), state.getCountryName()));
            return false;
        }
        return true;
    }

    private <T> boolean hasElements(List<T> list) {
        if (list.isEmpty()) {
            log.warn("Tried to upsert an empty list");
            return false;
        }
        return true;
    }

    private void updateCountry(Country country) {
        log.info("Country already exists - updating information");
        Integer result = geoLocationMapper.updateCountry(country);
        verifyUpdate(result, country.getName());
    }

    private void insertCountry(Country country) {
        log.info("Country does not exist - inserting information");
        Integer result = geoLocationMapper.insertCountry(country);
        verifyInsert(result, country.getName());
    }

    private void updateState(State state) {
        log.info("State already exists - updating information");
        Integer result = geoLocationMapper.updateState(state);
        verifyUpdate(result, state.getName());
    }

    private void insertState(State state) {
        log.info("State does not exist - inserting information");
        Integer result = geoLocationMapper.insertState(state);
        verifyInsert(result, state.getName());
    }

    private void updateCity(City city) {
        log.info("City already exists - updating information");
        Integer result = geoLocationMapper.updateCity(city);
        verifyUpdate(result, city.getName());
    }

    private void insertCity(City city) {
        log.info("City does not exist - inserting information");
        Integer result = geoLocationMapper.insertCity(city);
        verifyInsert(result, city.getName());
    }

    private void verifyUpdate(Integer result, String name) {
        if (!operationWasSuccessful(result)) {
            log.error(format("Error when updating %s", name));
            throw new UpdateException();
        }
    }

    private void verifyInsert(Integer result, String name) {
        if (!operationWasSuccessful(result)) {
            log.error(format("Error when inserting %s", name));
            throw new InsertException();
        }
    }

    private boolean operationWasSuccessful(Integer result) {
        return result == 1;
    }

    private List<String> getCountryNames() {
        return geoLocationMapper.findAllCountries().stream()
                .map(Country::getName)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
    }

    private List<String> getStateNames() {
        return geoLocationMapper.findAllStates().stream()
                .map(State::getName)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
    }
}
