package com.heliolicity.airqualityapi.service.impl;

import com.heliolicity.airqualityapi.client.AirVisualClient;
import com.heliolicity.airqualityapi.client.dto.CityDto;
import com.heliolicity.airqualityapi.client.dto.CountryDto;
import com.heliolicity.airqualityapi.client.dto.StateDto;
import com.heliolicity.airqualityapi.configuration.PollingConfiguration;
import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;
import com.heliolicity.airqualityapi.service.AirVisualService;
import com.heliolicity.airqualityapi.service.GeoLocationService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
@Service
public class AirVisualServiceImpl implements AirVisualService {

    //TODO: Add an aspect logger to log service calls

    private AirVisualClient airVisualClient;
    private GeoLocationService geoLocationService;
    private PollingConfiguration pollingConfiguration;

    @Autowired
    public AirVisualServiceImpl(AirVisualClient airVisualClient, GeoLocationService geoLocationService, PollingConfiguration pollingConfiguration) {
        this.airVisualClient = airVisualClient;
        this.geoLocationService = geoLocationService;
        this.pollingConfiguration = pollingConfiguration;
    }

    @Override
    public void refreshCountries() {
        List<CountryDto> countryDtos = getAllCountries();
        List<Country> countries = convertCountryDtosToCountries(countryDtos);

        if (!countries.isEmpty()) {
            log.info("Refreshing Countries");

            try {
                geoLocationService.upsertCountries(countries);
            } catch (Exception e) {
                log.error("Encountered error refreshing Countries");
                log.error(e.getMessage());
            }
        }
        else {
            log.warn("No Countries to refresh");
        }
    }

    @Override
    @Async
    public void refreshStates() {
        List<Country> countries = geoLocationService.getAllCountries();

        if (!countries.isEmpty()) {
            log.info("Refreshing States");

            for (Country country : countries) {
                try {
                    CompletableFuture<List<StateDto>> completableFutureStates = getAllStatesForCountry(country);
                    geoLocationService.upsertStates(convertStateDtosToStates(country, completableFutureStates.get()));
                } catch (InterruptedException e) {
                    log.error("Interrupted exception");
                    log.error(e.getMessage());
                } catch (ExecutionException e) {
                    log.error("Execution exception");
                    log.error(e.getMessage());
                } catch (Exception e) {
                    log.error("Encountered error refreshing States");
                    log.error(e.getMessage());
                }
            }
        }
        else {
            log.warn("No States to refresh");
        }
    }

    @Override
    @Async
    public void refreshCities() {
        List<State> states = geoLocationService.getAllStates();

        if (!states.isEmpty()) {
            log.info("Refreshing Cities");

            for (State state : states) {
                try {
                    CompletableFuture<List<CityDto>> completableFutureCities = getAllCitiesForStateAndCountry(state);
                    geoLocationService.upsertCities(convertCityDtosToCities(state, completableFutureCities.get()));
                } catch (InterruptedException e) {
                    log.error("Interrupted exception");
                    log.error(e.getMessage());
                } catch (ExecutionException e) {
                    log.error("Execution exception");
                    log.error(e.getMessage());
                } catch (Exception e) {
                    log.error("Encountered error refreshing Cities");
                    log.error(e.getMessage());
                }
            }
        }
        else {
            log.warn("No Cities to refresh");
        }
    }

    private List<CountryDto> getAllCountries() {
        log.info("In Service");
        return airVisualClient.getAllCountries();
    }

    private CompletableFuture<List<StateDto>> getAllStatesForCountry(Country country) throws InterruptedException {
        List<StateDto> stateDtos = airVisualClient.getAllStatesForCountry(country.getName());
        Thread.sleep(pollingConfiguration.getTimeBetweenRequests());
        return CompletableFuture.completedFuture(stateDtos);
    }

    private CompletableFuture<List<CityDto>> getAllCitiesForStateAndCountry(State state) throws InterruptedException {
        List<CityDto> cityDtos = airVisualClient.getAllCitiesForStateAndCountry(state.getName(), state.getCountryName());
        Thread.sleep(pollingConfiguration.getTimeBetweenRequests());
        return CompletableFuture.completedFuture(cityDtos);
    }

    private List<Country> convertCountryDtosToCountries(List<CountryDto> countryDtos) {
        List<Country> countries = new ArrayList<>();

        for (CountryDto countryDto : countryDtos) {
            countries.add(Country.builder().name(countryDto.getCountry()).build());
        }
        return countries;
    }

    private List<State> convertStateDtosToStates(Country country, List<StateDto> stateDtos) {
        List<State> states = new ArrayList<>();

        for (StateDto stateDto : stateDtos) {
            states.add(State.builder().countryId(country.getId()).name(stateDto.getState()).build());
        }
        return states;
    }

    private List<City> convertCityDtosToCities(State state, List<CityDto> cityDtos) {
        List<City> cities = new ArrayList<>();

        for (CityDto cityDto : cityDtos) {
            cities.add(City.builder().stateId(state.getId()).name(cityDto.getCity()).build());
        }
        return cities;
    }
}
