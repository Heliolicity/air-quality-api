package com.heliolicity.airqualityapi.service.impl;

import com.heliolicity.airqualityapi.configuration.KafkaTopicConfiguration;
import com.heliolicity.airqualityapi.exception.InvalidPingRequestException;
import com.heliolicity.airqualityapi.service.PingMessageService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import static java.lang.String.format;

@Slf4j
@Service
public class PingMessageServiceImpl implements PingMessageService {

    //TODO: Add an aspect logger to log service calls

    private KafkaTemplate kafkaTemplate;
    private KafkaTopicConfiguration kafkaTopicConfiguration;

    @Autowired
    public PingMessageServiceImpl(KafkaTemplate kafkaTemplate, KafkaTopicConfiguration kafkaTopicConfiguration) {
        this.kafkaTemplate = kafkaTemplate;
        this.kafkaTopicConfiguration = kafkaTopicConfiguration;
    }

    @Override
    public void sendMessage(String message) {
        log.info(format("Received following ping message %s", message));

        if (!isValidPingRequest(message)) {
            log.warn("Received invalid ping request");
            throw new InvalidPingRequestException();
        }
        log.info(format("Sending message=[%s]", message));
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(kafkaTopicConfiguration.getPingTopic(), message);
        //TODO: Return a response to the client on this future indicating success or failure of the Kafka request
        future.addCallback(buildListenableFutureCallback(message));
    }

    private boolean isValidPingRequest(String message) {
        return "ping".equalsIgnoreCase(message);
    }

    private ListenableFutureCallback<SendResult<String, String>> buildListenableFutureCallback(String message) {
        return new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info(format("Sent message=[%s] with offset=[%d]", message, result.getRecordMetadata().offset()));
            }
            @Override
            public void onFailure(Throwable ex) {
                log.info(format("Unable to send message=[%s] due to %s", message, ex.getMessage()));
            }
        };
    }
}
