package com.heliolicity.airqualityapi.domain;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.math.BigInteger;
import java.time.LocalDateTime;

@Builder
@Data
@ToString
public class City {

    private BigInteger id;
    private BigInteger stateId;
    private String stateName;
    private String name;
    private LocalDateTime lastModified;

}
