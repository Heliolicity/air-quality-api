package com.heliolicity.airqualityapi.domain;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.math.BigInteger;
import java.time.LocalDateTime;

@Builder
@Data
@ToString
public class Country {

    private BigInteger id;
    private String name;
    private LocalDateTime lastModified;

}
