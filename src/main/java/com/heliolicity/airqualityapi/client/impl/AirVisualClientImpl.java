package com.heliolicity.airqualityapi.client.impl;

import com.heliolicity.airqualityapi.client.AirVisualClient;
import com.heliolicity.airqualityapi.client.dto.CitiesList;
import com.heliolicity.airqualityapi.client.dto.CityDto;
import com.heliolicity.airqualityapi.client.dto.CountriesList;
import com.heliolicity.airqualityapi.client.dto.CountryDto;
import com.heliolicity.airqualityapi.client.dto.QueryParam;
import com.heliolicity.airqualityapi.client.dto.StateDto;
import com.heliolicity.airqualityapi.client.dto.StatesList;
import com.heliolicity.airqualityapi.configuration.AirVisualConfiguration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
public class AirVisualClientImpl implements AirVisualClient {

    private RestTemplate airVisualRestTemplate;
    private AirVisualConfiguration airVisualConfiguration;

    @Autowired
    public AirVisualClientImpl(RestTemplate airVisualRestTemplate, AirVisualConfiguration airVisualConfiguration) {
        this.airVisualRestTemplate = airVisualRestTemplate;
        this.airVisualConfiguration = airVisualConfiguration;
    }

    @Override
    public List<CountryDto> getAllCountries() {
        URI uri = buildURI(airVisualConfiguration.getCountriesApi(), airVisualConfiguration.getApiKey(), Collections.emptyList());

        if (Objects.isNull(uri)) {
            log.error("Error constructing URI");
            return new ArrayList<>();
        }

        try {
            CountriesList countriesList = airVisualRestTemplate.getForObject(uri, CountriesList.class);

            if (Objects.isNull(countriesList)) {
                log.error("Error - null object returned from AirVisual Countries API");
                return new ArrayList<>();
            }

            String status = countriesList.getStatus();

            if (!status.toUpperCase().equals("SUCCESS")) {
                log.error(String.format("Error - call to AirVisual Countries API was not successful: %s", status));
                return new ArrayList<>();
            }

            List<CountryDto> countries = Optional.ofNullable(countriesList.getData()).orElse(new ArrayList<>());

            if (countries.isEmpty()) {
                log.error("Error - no Countries could be discerned from AirVisual Countries API response");
            }
            countries.forEach(countryDto -> log.info(countryDto.getCountry() + "; "));
            return countries;
        }
        catch (Exception e) {
            log.error("Error connecting to AirVisual Countries endpoint: " + uri.toString());
            log.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<StateDto> getAllStatesForCountry(String countryName) {
        QueryParam queryParam = QueryParam.builder().key("country").value(countryName).build();
        URI uri = buildURI(airVisualConfiguration.getStatesApi(), airVisualConfiguration.getApiKey(), Collections.singletonList(queryParam));

        if (Objects.isNull(uri)) {
            log.error("Error constructing URI");
            return new ArrayList<>();
        }

        try {
            StatesList statesList = airVisualRestTemplate.getForObject(uri, StatesList.class);

            if (Objects.isNull(statesList)) {
                log.error("Error - null object returned from AirVisual States API");
                return new ArrayList<>();
            }

            String status = statesList.getStatus();

            if (!status.toUpperCase().equals("SUCCESS")) {
                log.error(String.format("Error - call to AirVisual States API was not successful: %s", status));
                return new ArrayList<>();
            }

            List<StateDto> states = Optional.ofNullable(statesList.getData()).orElse(new ArrayList<>());

            if (states.isEmpty()) {
                log.error("Error - no States could be discerned from AirVisual States API response");
            }
            states.forEach(stateDto -> log.info(stateDto.getState() + "; "));
            return states;
        }
        catch (Exception e) {
            log.error("Error connecting to AirVisual States endpoint: " + uri.toString());
            log.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<CityDto> getAllCitiesForStateAndCountry(String stateName, String countryName) {
        List<QueryParam> queryParams = buildQueryParams(stateName, countryName);
        URI uri = buildURI(airVisualConfiguration.getCitiesApi(), airVisualConfiguration.getApiKey(), queryParams);

        if (Objects.isNull(uri)) {
            log.error("Error constructing URI");
            return new ArrayList<>();
        }

        try {
            CitiesList citiesList = airVisualRestTemplate.getForObject(uri, CitiesList.class);

            if (Objects.isNull(citiesList)) {
                log.error("Error - null object returned from AirVisual Cities API");
                return new ArrayList<>();
            }

            String status = citiesList.getStatus();

            if (!status.toUpperCase().equals("SUCCESS")) {
                log.error(String.format("Error - call to AirVisual Cities API was not successful: %s", status));
                return new ArrayList<>();
            }

            List<CityDto> cities = Optional.ofNullable(citiesList.getData()).orElse(new ArrayList<>());

            if (cities.isEmpty()) {
                log.error("Error - no States could be discerned from AirVisual States API response");
            }
            cities.forEach(cityDto -> log.info(cityDto.getCity() + "; "));
            return cities;
        }
        catch (Exception e) {
            log.error("Error connecting to AirVisual Cities endpoint: " + uri.toString());
            log.error(e.getMessage());
            return new ArrayList<>();
        }
    }

    private List<QueryParam> buildQueryParams(String stateName, String countryName) {
        QueryParam stateQueryParam = QueryParam.builder().key("state").value(stateName).build();
        QueryParam countryQueryParam = QueryParam.builder().key("country").value(countryName).build();
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(stateQueryParam);
        queryParams.add(countryQueryParam);
        return queryParams;
    }

    private URI buildURI(String api, String key, List<QueryParam> queryParams) {
        try {
            if (queryParams.isEmpty()) {
                return new URI(api + "?key=" + key);
            }
            StringBuilder uriBuilder = new StringBuilder(api + "?key=" + key + "&");

            for (QueryParam queryParam : queryParams) {
                uriBuilder.append(queryParam.getKey())
                    .append("=")
                    .append(URLEncoder.encode(queryParam.getValue(), "UTF-8"))
                    .append("&");
            }

            String uri = uriBuilder.toString();

            if (uri.endsWith("&")) {
                uri = uri.substring(0, uri.length() - 1);
            }
            return new URI(uri);
        }
        catch (URISyntaxException use) {
            log.error(String.format("URI Syntax Exception: %s", api + "?key=" + key));
            log.error(use.getMessage());
            return null;
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
