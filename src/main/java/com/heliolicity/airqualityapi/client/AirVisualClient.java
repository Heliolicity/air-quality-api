package com.heliolicity.airqualityapi.client;

import com.heliolicity.airqualityapi.client.dto.CityDto;
import com.heliolicity.airqualityapi.client.dto.CountryDto;
import com.heliolicity.airqualityapi.client.dto.StateDto;

import java.util.List;

public interface AirVisualClient {

    List<CountryDto> getAllCountries();

    List<StateDto> getAllStatesForCountry(String countryName);

    List<CityDto> getAllCitiesForStateAndCountry(String stateName, String countryName);

}
