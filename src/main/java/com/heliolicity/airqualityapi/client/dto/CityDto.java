package com.heliolicity.airqualityapi.client.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CityDto {

    private String city;

}
