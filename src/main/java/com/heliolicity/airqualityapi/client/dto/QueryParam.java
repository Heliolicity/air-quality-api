package com.heliolicity.airqualityapi.client.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class QueryParam {

    private String key;
    private String value;

}
