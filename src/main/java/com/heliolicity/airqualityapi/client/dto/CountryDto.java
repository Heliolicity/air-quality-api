package com.heliolicity.airqualityapi.client.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CountryDto {

    private String country;

}
