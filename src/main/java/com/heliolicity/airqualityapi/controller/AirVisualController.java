package com.heliolicity.airqualityapi.controller;

import com.heliolicity.airqualityapi.service.AirVisualService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/airvisual")
public class AirVisualController {

    private AirVisualService airVisualService;

    @Autowired
    public AirVisualController(AirVisualService airVisualService) {
        this.airVisualService = airVisualService;
    }

    @PutMapping("/refreshcountries")
    public void refreshCountries() {
        log.info("Refreshing Countries from AirVisual API");
        airVisualService.refreshCountries();
    }

    @PutMapping("/refreshstates")
    public void refreshStates() {
        log.info("Refreshing States from AirVisual API");
        airVisualService.refreshStates();
    }

    @PutMapping("/refreshcities")
    public void refreshCities() {
        log.info("Refreshing Cities from AirVisual API");
        airVisualService.refreshCities();
    }
}
