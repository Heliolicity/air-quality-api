package com.heliolicity.airqualityapi.controller.validator;

import com.heliolicity.airqualityapi.domain.Country;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class CountryValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Country.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "error.name", "Country name cannot be blank or null");
    }
}
