package com.heliolicity.airqualityapi.controller;

import com.heliolicity.airqualityapi.service.PingMessageService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/ping")
@Validated
public class PingMessageController {

    private PingMessageService pingMessageService;

    @Autowired
    public PingMessageController(PingMessageService pingMessageService) {
        this.pingMessageService = pingMessageService;
    }

    @PostMapping
    public void pingKafka(@Validated @RequestParam("message") String message) {
        log.info("Received request to ping Kafka");
        pingMessageService.sendMessage(message);
    }
}