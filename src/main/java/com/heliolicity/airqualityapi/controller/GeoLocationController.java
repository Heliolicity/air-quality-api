package com.heliolicity.airqualityapi.controller;

import com.heliolicity.airqualityapi.controller.validator.CountryValidator;
import com.heliolicity.airqualityapi.controller.validator.StateValidator;
import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;
import com.heliolicity.airqualityapi.service.GeoLocationService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
@Validated
public class GeoLocationController {

    private GeoLocationService geoLocationService;
    private CountryValidator countryValidator;
    private StateValidator stateValidator;

    @InitBinder("country")
    public void initCountryBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(countryValidator);
    }

    @InitBinder("state")
    public void initStateBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(stateValidator);
    }

    @InitBinder("city")
    public void initCityBinder(WebDataBinder webDataBinder) { webDataBinder.setValidator(countryValidator); }

    @Autowired
    public GeoLocationController(GeoLocationService geoLocationService, CountryValidator countryValidator, StateValidator stateValidator) {
        this.geoLocationService = geoLocationService;
        this.countryValidator = countryValidator;
        this.stateValidator = stateValidator;
    }

    @GetMapping("/countries")
    public List<Country> getAllCountries() {
        log.info("Getting a list of all supported Countries");
        return geoLocationService.getAllCountries();
    }

    @GetMapping("/states")
    public List<State> getAllStates() {
        log.info("Getting a list of all supported States");
        return geoLocationService.getAllStates();
    }

    @GetMapping("/statesforcountry")
    public List<State> getAllStatesForCountry(@Valid @RequestBody Country country) {
        log.info(String.format("Getting a list of all supported States for Country %s", country.getName()));
        return geoLocationService.getAllStatesForCountry(country);
    }

    @GetMapping("/cities")
    public List<City> getAllCities() {
        log.info("Getting a list of all supported Cities");
        return geoLocationService.getAllCities();
    }

    @GetMapping("/citiesforstate")
    public List<City> getAllCitiesForState(@Valid @RequestBody State state) {
        log.info(String.format("Getting a list of all supported Cities for State %s", state.getName()));
        return geoLocationService.getAllCitiesForState(state);
    }

    @GetMapping("/citiesforcountry")
    public List<City> getAllCitiesForCountry(@RequestBody Country country) {
        log.info(String.format("Getting a list of all supported Cities for Country %s", country.getName()));
        return geoLocationService.getAllCitiesForCountry(country);
    }
}
