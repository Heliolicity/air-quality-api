package com.heliolicity.airqualityapi.configuration;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Slf4j
@Configuration
@ConfigurationProperties(prefix = "airvisual.apis")
@Data
@Validated
public class AirVisualConfiguration {

    @NotNull(message = "apiKey for AirVisual API cannot be null")
    @NotBlank(message = "apiKey for AirVisual API cannot be null")
    private String apiKey;

    @NotNull(message = "countriesApi for AirVisual API cannot be null")
    @NotBlank(message = "countriesApi for AirVisual API cannot be null")
    private String countriesApi;

    @NotNull(message = "statesApi for AirVisual API cannot be null")
    @NotBlank(message = "statesApi for AirVisual API cannot be null")
    private String statesApi;

    @NotNull(message = "citiesApi for AirVisual API cannot be null")
    @NotBlank(message = "citiesApi for AirVisual API cannot be null")
    private String citiesApi;

    @PostConstruct
    private void displayValuesOnStartup() {
        log.info(String.format("apiKey is %s", apiKey));
        log.info(String.format("countriesApi is %s", countriesApi));
        log.info(String.format("statesApi is %s", statesApi));
        log.info(String.format("citiesApi is %s", citiesApi));
    }
}
