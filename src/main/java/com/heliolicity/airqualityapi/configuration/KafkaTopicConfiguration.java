package com.heliolicity.airqualityapi.configuration;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
@ConfigurationProperties(prefix = "kafka")
@Data
public class KafkaTopicConfiguration {

    @NotNull(message = "bootstrapAddress cannot be null")
    @NotBlank(message = "bootstrapAddress cannot be blank")
    private String bootstrapAddress;

    @NotNull(message = "pingTopic cannot be null")
    @NotBlank(message = "pingTopic cannot be blank")
    private String pingTopic;

    @NotNull(message = "reconnectBackoffMs cannot be null")
    @NotBlank(message = "reconnectBackoffMs cannot be blank")
    @Value("${kafka.reconnect.backoff.ms}")
    private String reconnectBackoffMs;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configurations = new HashMap<>();
        configurations.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        configurations.put(AdminClientConfig.RECONNECT_BACKOFF_MS_CONFIG, reconnectBackoffMs);
        return new KafkaAdmin(configurations);
    }

    @Bean
    public NewTopic pingTopic() {
        return new NewTopic(pingTopic, 1, (short) 1);
    }

    @PostConstruct
    private void displayOnStartup() {
        log.info(String.format("bootstrapAddress is %s", bootstrapAddress));
        log.info(String.format("reconnectBackoffMs is %s", reconnectBackoffMs));
    }
}