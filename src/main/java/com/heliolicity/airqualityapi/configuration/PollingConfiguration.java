package com.heliolicity.airqualityapi.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Configuration
@ConfigurationProperties(prefix = "polling")
@Data
public class PollingConfiguration {

    @NotNull(message = "timeBetweenRequests cannot be null")
    private Long timeBetweenRequests;

}
