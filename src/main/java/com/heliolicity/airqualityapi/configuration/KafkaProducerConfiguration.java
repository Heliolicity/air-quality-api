package com.heliolicity.airqualityapi.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class KafkaProducerConfiguration {

    private KafkaTopicConfiguration kafkaTopicConfiguration;

    @Autowired
    public KafkaProducerConfiguration(KafkaTopicConfiguration kafkaTopicConfiguration) {
        this.kafkaTopicConfiguration = kafkaTopicConfiguration;
    }

    @Bean
    public ProducerFactory<String, String> producerFactory() {
        Map<String, Object> configurationProperties = new HashMap<>();
        configurationProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaTopicConfiguration.getBootstrapAddress());
        configurationProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configurationProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configurationProperties.put(ProducerConfig.RECONNECT_BACKOFF_MS_CONFIG, kafkaTopicConfiguration.getReconnectBackoffMs());
        return new DefaultKafkaProducerFactory<>(configurationProperties);
    }

    @Bean
    public KafkaTemplate kafkaTemplate() {
        return new KafkaTemplate(producerFactory());
    }

    @PostConstruct
    private void displayOnStartup() {
        log.info(String.format("bootstrapAddress is %s", kafkaTopicConfiguration.getBootstrapAddress()));
        log.info(String.format("reconnectBackoffMs is %s", kafkaTopicConfiguration.getReconnectBackoffMs()));
    }
}