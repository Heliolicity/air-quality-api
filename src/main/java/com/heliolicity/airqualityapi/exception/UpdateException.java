package com.heliolicity.airqualityapi.exception;

public class UpdateException extends RuntimeException {

    public UpdateException() {
        super("Database exception when updating record");
    }

}
