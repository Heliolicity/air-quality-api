package com.heliolicity.airqualityapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class InvalidPingRequestException extends RuntimeException {

    public InvalidPingRequestException() {
        super("Invalid ping request");
    }

}
