package com.heliolicity.airqualityapi.exception;

public class InsertException extends RuntimeException {

    public InsertException() {
        super("Database exception when inserting record");
    }

}
