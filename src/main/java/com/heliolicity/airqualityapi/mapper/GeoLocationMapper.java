package com.heliolicity.airqualityapi.mapper;

import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Mapper
public interface GeoLocationMapper {

    @Results(id = "countries", value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "lastModified", column = "LAST_MODIFIED")
    })
    @Select("SELECT ID, NAME, LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_COUNTRIES")
    List<Country> findAllCountries();

    @ResultMap("countries")
    @Select("<script>" +
            "SELECT ID, NAME, LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_COUNTRIES " +
            "WHERE UPPER(NAME) = UPPER(#{countryName}) " +
            "</script>")
    Optional<Country> findCountryByName(@Param("countryName") String countryName);

    @Results(id = "states", value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "countryId", column = "COUNTRY_ID"),
            @Result(property = "countryName", column = "COUNTRY_NAME"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "lastModified", column = "LAST_MODIFIED")
    })
    @Select("SELECT STATES.ID, STATES.COUNTRY_ID, COUNTRIES.NAME AS COUNTRY_NAME, STATES.NAME, STATES.LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_STATES STATES " +
            "INNER JOIN AQ_APP_DB.AQ_COUNTRIES COUNTRIES ON STATES.COUNTRY_ID = COUNTRIES.ID")
    List<State> findAllStates();

    @ResultMap("states")
    @Select("SELECT STATES.ID, STATES.COUNTRY_ID, COUNTRIES.NAME AS COUNTRY_NAME, STATES.NAME, STATES.LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_STATES STATES " +
            "INNER JOIN AQ_APP_DB.AQ_COUNTRIES COUNTRIES ON STATES.COUNTRY_ID = COUNTRIES.ID " +
            "WHERE UPPER(COUNTRIES.NAME) = UPPER(#{country.name})")
    List<State> findAllStatesForCountry(@Param("country") Country country);

    @ResultMap("states")
    @Select("SELECT STATES.ID, STATES.COUNTRY_ID, COUNTRIES.NAME AS COUNTRY_NAME, STATES.NAME, STATES.LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_STATES STATES " +
            "INNER JOIN AQ_APP_DB.AQ_COUNTRIES COUNTRIES ON STATES.COUNTRY_ID = COUNTRIES.ID " +
            "WHERE UPPER(STATES.NAME) = UPPER(#{stateName})")
    Optional<State> findStateByName(@Param("stateName") String stateName);

    @Results(id = "cities", value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "stateId", column = "STATE_ID"),
            @Result(property = "stateName", column = "STATE_NAME"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "lastModified", column = "LAST_MODIFIED")
    })
    @Select("SELECT CITIES.ID, CITIES.STATE_ID, STATES.NAME AS STATE_NAME, CITIES.NAME, CITIES.LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_CITIES CITIES " +
            "INNER JOIN AQ_APP_DB.AQ_STATES STATES ON CITIES.STATE_ID = STATES.ID")
    List<City> findAllCities();

    @ResultMap("cities")
    @Select("SELECT CITIES.ID, CITIES.STATE_ID, STATES.NAME AS STATES_NAME, CITIES.NAME, CITIES.LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_CITIES CITIES " +
            "INNER JOIN AQ_APP_DB.AQ_STATES STATES ON CITIES.STATE_ID = STATES.ID " +
            "WHERE UPPER(STATES.NAME) = UPPER(#{state.name})")
    List<City> findAllCitiesForState(@Param("state") State state);

    @ResultMap("cities")
    @Select("SELECT CITIES.ID, CITIES.STATE_ID, STATES.NAME AS STATES_NAME, CITIES.NAME, CITIES.LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_CITIES CITIES " +
            "INNER JOIN AQ_APP_DB.AQ_STATES STATES ON CITIES.STATE_ID = STATES.ID " +
            "INNER JOIN AQ_APP_DB.AQ_COUNTRIES COUNTRIES ON STATES.COUNTRY_ID = COUNTRIES.ID " +
            "WHERE UPPER(COUNTRIES.NAME) = UPPER(#{country.name})")
    List<City> findAllCitiesForCountry(@Param("country") Country country);

    @ResultMap("cities")
    @Select("<script>" +
            "SELECT CITIES.ID, CITIES.STATE_ID, STATES.NAME AS STATES_NAME, CITIES.NAME, CITIES.LAST_MODIFIED " +
            "FROM AQ_APP_DB.AQ_CITIES CITIES " +
            "INNER JOIN AQ_APP_DB.AQ_STATES STATES ON CITIES.STATE_ID = STATES.ID " +
            "INNER JOIN AQ_APP_DB.AQ_COUNTRIES COUNTRIES ON STATES.COUNTRY_ID = COUNTRIES.ID " +
            "WHERE UPPER(CITIES.NAME) = UPPER(#{cityName})" +
            "</script>")
    Optional<City> findCityByName(@Param("cityName") String cityName);

    @Insert("<script>" +
            "INSERT INTO AQ_APP_DB.AQ_COUNTRIES (NAME, LAST_MODIFIED) VALUES " +
            "(#{country.name}, NOW())" +
            "</script>")
    Integer insertCountry(@Param("country") Country country);

    @Update("<script>" +
            "UPDATE AQ_APP_DB.AQ_COUNTRIES " +
            "SET NAME = #{country.name}, " +
                "LAST_MODIFIED = NOW() " +
            "WHERE ID = #{country.id}" +
            "</script>")
    Integer updateCountry(@Param("country") Country country);

    @Deprecated
    @Insert("<script>" +
            "INSERT INTO AQ_APP_DB.AQ_COUNTRIES (NAME, LAST_MODIFIED) VALUES " +
                "<foreach collection=\"countries\" item=\"element\" index=\"index\" open=\"(\" close=\")\" separator=\") , (\">" +
                    "#{element.name}, " +
                    "NOW()" +
                "</foreach> " +
            "ON CONFLICT ON CONSTRAINT NAME_UNIQUE_CON DO NOTHING " +
            "</script>")
    Integer upsertCountries(@Param("countries") List<Country> countries);

    @Insert("<script>" +
            "INSERT INTO AQ_APP_DB.AQ_STATES (COUNTRY_ID, NAME, LAST_MODIFIED) VALUES " +
            "(#{state.countryId}, #{state.name}, NOW())" +
            "</script>")
    Integer insertState(@Param("state") State state);

    @Update("<script>" +
            "UPDATE AQ_APP_DB.AQ_STATES " +
            "SET COUNTRY_ID = #{state.countryId}, " +
                "NAME = #{state.name}, " +
                "LAST_MODIFIED = NOW() " +
            "WHERE ID = #{state.id}" +
            "</script>")
    Integer updateState(@Param("state") State state);

    @Deprecated
    @Insert("<script>" +
            "INSERT INTO AQ_APP_DB.AQ_STATES (COUNTRY_ID, NAME, LAST_MODIFIED) VALUES " +
                "<foreach collection=\"states\" item=\"element\" index=\"index\" separator=\",\">" +
                    "(#{element.countryId}, " +
                    "#{element.name}, " +
                    "NOW())" +
                "</foreach> " +
            "ON CONFLICT ON CONSTRAINT STATE_UNIQUE_CON DO NOTHING " +
            "</script>")
    Integer upsertStates(@Param("states") List<State> states);

    @Insert("<script>" +
            "INSERT INTO AQ_APP_DB.AQ_CITIES (STATE_ID, NAME, LAST_MODIFIED) VALUES " +
            "(#{city.stateId}, #{city.name}, NOW())" +
            "</script>")
    Integer insertCity(@Param("city") City city);

    @Update("<script>" +
            "UPDATE AQ_APP_DB.AQ_CITIES " +
            "SET STATE_ID = #{city.stateId}, " +
                "NAME = #{city.name}, " +
                "LAST_MODIFIED = NOW() " +
            "WHERE ID = #{city.id}" +
            "</script>")
    Integer updateCity(@Param("city") City city);

    @Deprecated
    @Insert("<script>" +
                "INSERT INTO AQ_APP_DB.AQ_CITIES (STATE_ID, NAME, LAST_MODIFIED) VALUES " +
                    "<foreach collection=\"cities\" item=\"element\" index=\"index\" separator=\",\">" +
                        "(#{element.stateId}, " +
                        "#{element.name}, " +
                        "NOW())" +
                    "</foreach> " +
                "ON CONFLICT ON CONSTRAINT CITY_UNIQUE_CON DO NOTHING " +
            "</script>")
    Integer upsertCities(@Param("cities") List<City> cities);
}
