DROP SCHEMA IF EXISTS aq_app_db CASCADE;

CREATE SCHEMA aq_app_db;

DROP TABLE IF EXISTS aq_app_db.aq_countries;

DROP SEQUENCE IF EXISTS aq_app_db.seq_countries;

CREATE SEQUENCE aq_app_db.seq_countries START WITH 3 INCREMENT BY 1;

CREATE TABLE aq_app_db.aq_countries (
    id            bigint         DEFAULT aq_app_db.seq_countries.nextval primary key,
    name          varchar(400)   not null,
    last_modified timestamp
);

DROP SEQUENCE IF EXISTS aq_app_db.seq_states;

CREATE SEQUENCE aq_app_db.seq_states START WITH 5 INCREMENT BY 1;

DROP TABLE IF EXISTS aq_app_db.aq_states;

CREATE TABLE aq_app_db.aq_states (
    id            bigint        DEFAULT aq_app_db.seq_states.nextval  primary key,
    country_id    bigint        not null,
    name          varchar(400)  not null,
    last_modified timestamp
);

DROP SEQUENCE IF EXISTS aq_app_db.seq_cities;

CREATE SEQUENCE aq_app_db.seq_cities START WITH 5 INCREMENT BY 1;

DROP TABLE IF EXISTS aq_app_db.aq_cities;

CREATE TABLE aq_app_db.aq_cities (
    id            bigint        DEFAULT aq_app_db.seq_cities.nextval primary key,
    state_id      bigint        not null,
    name          varchar(400)  not null,
    last_modified timestamp
);

ALTER TABLE aq_app_db.aq_countries ADD CONSTRAINT NAME_UNIQUE_CON UNIQUE (name);