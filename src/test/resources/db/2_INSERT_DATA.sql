INSERT INTO aq_app_db.aq_countries VALUES (1, 'Atlantis', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));
INSERT INTO aq_app_db.aq_countries VALUES (2, 'Val Verde', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));

INSERT INTO aq_app_db.aq_states VALUES (1, 1, 'Some State In Atlantis', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));
INSERT INTO aq_app_db.aq_states VALUES (2, 1, 'Some Other State In Atlantis', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));
INSERT INTO aq_app_db.aq_states VALUES (3, 2, 'Some State In Val Verde', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));
INSERT INTO aq_app_db.aq_states VALUES (4, 2, 'Some Other State In Val Verde', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));

INSERT INTO aq_app_db.aq_cities VALUES (1, 1, 'Some City In Some State In Atlantis', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));
INSERT INTO aq_app_db.aq_cities VALUES (2, 1, 'Some Other City In Some State In Atlantis', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));
INSERT INTO aq_app_db.aq_cities VALUES (3, 2, 'Some Other City In Some Other State In Atlantis', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));
INSERT INTO aq_app_db.aq_cities VALUES (4, 3, 'Some City In Some State In Val Verde', TO_TIMESTAMP('2019-06-30', 'yyyy-MM-dd'));