package com.heliolicity.airqualityapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class AirQualityApiApplicationTests {

    @Test
    public void contextLoads() {
    }
}
