package com.heliolicity.airqualityapi;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@ComponentScan({
        "com.heliolicity.airqualityapi.configuration",
        "com.heliolicity.airqualityapi.controller",
        "com.heliolicity.airqualityapi.controller.validator",
        "com.heliolicity.airqualityapi.client",
        "com.heliolicity.airqualityapi.client.dto",
        "com.heliolicity.airqualityapi.client.impl",
        "com.heliolicity.airqualityapi.domain",
        "com.heliolicity.airqualityapi.service",
        "com.heliolicity.airqualityapi.service.impl"
})
@MapperScan("com.heliolicity.airqualityapi.mapper")
@MybatisTest
@SpringBootTest
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
