package com.heliolicity.airqualityapi.controller;

import com.heliolicity.airqualityapi.controller.validator.CountryValidator;
import com.heliolicity.airqualityapi.controller.validator.StateValidator;
import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;
import com.heliolicity.airqualityapi.service.impl.GeoLocationServiceImpl;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.bind.WebDataBinder;

import java.util.List;

import static com.heliolicity.airqualityapi.util.TestUtil.buildCities;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCitiesForCountry;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCitiesForState;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountries;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStates;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStatesForCountry;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class GeoLocationControllerTest {

    @InjectMocks
    private GeoLocationController geoLocationController;

    @Mock
    private GeoLocationServiceImpl geoLocationService;

    @Mock
    private CountryValidator countryValidator;

    @Mock
    private StateValidator stateValidator;

    @Mock
    private WebDataBinder webDataBinder;

    @Ignore
    @Test
    public void shouldInitCountryBinder() {
        geoLocationController.initCountryBinder(webDataBinder);

        assertThat(webDataBinder.getValidators()).contains(countryValidator);
    }

    @Ignore
    @Test
    public void shouldInitStateBinder() {

    }

    @Ignore
    @Test
    public void shouldInitCityBinder() {

    }

    @Test
    public void shouldGetAllCountries() {
        given(geoLocationService.getAllCountries()).willReturn(buildCountries());

        List<Country> countries = geoLocationController.getAllCountries();

        assertThat(countries).isEqualTo(buildCountries());
    }

    @Test
    public void shouldGetAllStates() {
        given(geoLocationService.getAllStates()).willReturn(buildStates());

        List<State> states = geoLocationController.getAllStates();

        assertThat(states).isEqualTo(buildStates());
    }

    @Test
    public void shouldGetAllStatesForCountry() {
        given(geoLocationService.getAllStatesForCountry(Country.builder().name("Atlantis").build())).willReturn(buildStatesForCountry());

        List<State> states = geoLocationController.getAllStatesForCountry(Country.builder().name("Atlantis").build());

        assertThat(states).isEqualTo(buildStatesForCountry());
    }

    @Test
    public void shouldGetAllCities() {
        given(geoLocationService.getAllCities()).willReturn(buildCities());

        List<City> cities = geoLocationController.getAllCities();

        assertThat(cities).isEqualTo(buildCities());
    }

    @Test
    public void shouldGetAllCitiesForState() {
        given(geoLocationService.getAllCitiesForState(State.builder().name("Some State In Atlantis").build())).willReturn(buildCitiesForState());

        List<City> cities = geoLocationController.getAllCitiesForState(State.builder().name("Some State In Atlantis").build());

        assertThat(cities).isEqualTo(buildCitiesForState());
    }

    @Test
    public void shouldGetAllCitiesForCountry() {
        given(geoLocationService.getAllCitiesForCountry(Country.builder().name("Atlantis").build())).willReturn(buildCitiesForCountry());

        List<City> cities = geoLocationController.getAllCitiesForCountry(Country.builder().name("Atlantis").build());

        assertThat(cities).isEqualTo(buildCitiesForCountry());
    }
}
