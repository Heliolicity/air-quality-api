package com.heliolicity.airqualityapi.controller;

import com.heliolicity.airqualityapi.service.impl.AirVisualServiceImpl;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.BDDMockito.then;

@RunWith(MockitoJUnitRunner.class)
public class AirVisualControllerTest {

    @InjectMocks
    private AirVisualController airVisualController;

    @Mock
    private AirVisualServiceImpl airVisualService;

    @Test
    public void shouldRefreshCountries() {
        airVisualController.refreshCountries();

        then(airVisualService).should().refreshCountries();
    }

    @Test
    public void shouldRefreshStates() {
        airVisualController.refreshStates();

        then(airVisualService).should().refreshStates();
    }

    @Test
    public void shouldRefreshCities() {
        airVisualController.refreshCities();

        then(airVisualService).should().refreshCities();
    }

    @After
    public void tearDown() {
        then(airVisualService).shouldHaveNoMoreInteractions();
    }
}
