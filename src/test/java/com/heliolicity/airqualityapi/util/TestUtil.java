package com.heliolicity.airqualityapi.util;

import com.heliolicity.airqualityapi.client.dto.CitiesList;
import com.heliolicity.airqualityapi.client.dto.CityDto;
import com.heliolicity.airqualityapi.client.dto.CountriesList;
import com.heliolicity.airqualityapi.client.dto.CountryDto;
import com.heliolicity.airqualityapi.client.dto.StateDto;
import com.heliolicity.airqualityapi.client.dto.StatesList;
import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;

import org.assertj.core.util.Lists;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

public class TestUtil {

    public static List<Country> buildCountries() {
        Country first = Country.builder().id(BigInteger.ONE).name("Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        Country second = Country.builder().id(BigInteger.valueOf(2)).name("Val Verde").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        return Lists.newArrayList(first, second);
    }

    public static Country buildCountry(String countryName) {
        return Country.builder().id(BigInteger.ONE).name(countryName).lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
    }

    public static List<State> buildStates() {
        State first = State.builder().id(BigInteger.ONE).countryId(BigInteger.ONE).countryName("Atlantis").name("Some State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        State second = State.builder().id(BigInteger.valueOf(2)).countryId(BigInteger.ONE).countryName("Atlantis").name("Some Other State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        State third = State.builder().id(BigInteger.valueOf(3)).countryId(BigInteger.valueOf(2)).countryName("Val Verde").name("Some State In Val Verde").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        State fourth = State.builder().id(BigInteger.valueOf(4)).countryId(BigInteger.valueOf(2)).countryName("Val Verde").name("Some Other State In Val Verde").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        return Lists.newArrayList(first, second, third, fourth);
    }

    public static List<State> buildStatesForCountry() {
        State first = State.builder().id(BigInteger.ONE).countryId(BigInteger.ONE).countryName("Atlantis").name("Some State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        State second = State.builder().id(BigInteger.valueOf(2)).countryId(BigInteger.ONE).countryName("Atlantis").name("Some Other State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        return Lists.newArrayList(first, second);
    }

    public static List<City> buildCities() {
        City first = City.builder().id(BigInteger.ONE).stateId(BigInteger.ONE).stateName("Some State In Atlantis").name("Some City In Some State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        City second = City.builder().id(BigInteger.valueOf(2)).stateId(BigInteger.ONE).stateName("Some State In Atlantis").name("Some Other City In Some State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        City third = City.builder().id(BigInteger.valueOf(3)).stateId(BigInteger.valueOf(2)).stateName("Some Other State In Atlantis").name("Some Other City In Some Other State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        City fourth = City.builder().id(BigInteger.valueOf(4)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Some State In Val Verde").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        return Lists.newArrayList(first, second, third, fourth);
    }

    public static List<City> buildCitiesForState() {
        City first = City.builder().id(BigInteger.ONE).stateId(BigInteger.ONE).stateName("Some State In Atlantis").name("Some City In Some State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        City second = City.builder().id(BigInteger.valueOf(2)).stateId(BigInteger.ONE).stateName("Some State In Atlantis").name("Some Other City In Some State In Atlantis").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        return Lists.newArrayList(first, second);
    }

    public static List<City> buildCitiesForCountry() {
        return Lists.newArrayList(City.builder().id(BigInteger.valueOf(4)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Some State In Val Verde").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build());
    }

    public static List<Country> buildCountriesToUpsert() {
        Country first = Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        Country second = Country.builder().id(BigInteger.valueOf(4)).name("Narnia").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        return Lists.newArrayList(first, second);
    }

    public static List<State> buildStatesToUpsert() {
        State first = State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        State second = State.builder().id(BigInteger.valueOf(4)).countryName("Shangri-La").name("Harmony").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        return Lists.newArrayList(first, second);
    }

    public static List<City> buildCitiesToUpsert() {
        City first = City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        City second = City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build();
        return Lists.newArrayList(first, second);
    }

    public static List<CountryDto> buildCountryDtos() {
        CountryDto first = new CountryDto();
        first.setCountry("Atlantis");
        CountryDto second = new CountryDto();
        second.setCountry("Val Verde");
        return Lists.newArrayList(first, second);
    }

    public static List<StateDto> buildStateDtos() {
        StateDto first = new StateDto();
        first.setState("Some State In Atlantis");
        return Lists.newArrayList(first);
    }

    public static List<CityDto> buildCityDtos() {
        CityDto first = new CityDto();
        first.setCity("Some City In Some State In Atlantis");
        return Lists.newArrayList(first);
    }

    public static CountriesList buildCountriesList(String status, List<CountryDto> data) {
        CountriesList countriesList = new CountriesList();
        countriesList.setStatus(status);
        countriesList.setData(data);
        return countriesList;
    }

    public static StatesList buildStatesList(String status, List<StateDto> data) {
        StatesList statesList = new StatesList();
        statesList.setStatus(status);
        statesList.setData(data);
        return statesList;
    }

    public static CitiesList buildCitiesList(String status, List<CityDto> data) {
        CitiesList citiesList = new CitiesList();
        citiesList.setStatus(status);
        citiesList.setData(data);
        return citiesList;
    }
}
