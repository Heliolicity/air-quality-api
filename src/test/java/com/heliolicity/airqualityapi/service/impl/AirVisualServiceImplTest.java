package com.heliolicity.airqualityapi.service.impl;

import com.heliolicity.airqualityapi.client.impl.AirVisualClientImpl;
import com.heliolicity.airqualityapi.configuration.PollingConfiguration;
import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static com.heliolicity.airqualityapi.util.TestUtil.buildCityDtos;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountryDtos;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStateDtos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AirVisualServiceImplTest {

    @InjectMocks
    private AirVisualServiceImpl airVisualService;

    @Mock
    private AirVisualClientImpl airVisualClient;

    @Mock
    private GeoLocationServiceImpl geoLocationService;

    @Mock
    private PollingConfiguration pollingConfiguration;

    @Captor
    private ArgumentCaptor<List<Country>> countriesArgumentCaptor;

    @Captor
    private ArgumentCaptor<List<State>> statesArgumentCaptor;

    @Captor
    private ArgumentCaptor<List<City>> citiesArgumentCaptor;

    @Before
    public void setUp() {
        given(pollingConfiguration.getTimeBetweenRequests()).willReturn(5000L);
    }

    @Test
    public void shouldRefreshCountries() {
        given(airVisualClient.getAllCountries()).willReturn(buildCountryDtos());

        airVisualService.refreshCountries();

        then(airVisualClient).should(times(1)).getAllCountries();
        then(geoLocationService).should(times(1)).upsertCountries(countriesArgumentCaptor.capture());
        then(airVisualClient).shouldHaveNoMoreInteractions();
        then(geoLocationService).shouldHaveNoMoreInteractions();
        then(pollingConfiguration).shouldHaveNoMoreInteractions();
        assertThat(countriesArgumentCaptor.getValue()).isEqualTo(Lists.newArrayList(
                Country.builder().name("Atlantis").build(),
                Country.builder().name("Val Verde").build()
        ));
    }

    @Test
    public void shouldNotRefreshCountries() {
        given(airVisualClient.getAllCountries()).willReturn(Lists.emptyList());

        airVisualService.refreshCountries();

        then(airVisualClient).should(times(1)).getAllCountries();
        then(airVisualClient).shouldHaveNoMoreInteractions();
        then(pollingConfiguration).shouldHaveZeroInteractions();
        then(geoLocationService).shouldHaveZeroInteractions();
    }

    @Test
    public void shouldRefreshStates() {
        given(airVisualClient.getAllStatesForCountry("Atlantis")).willReturn(buildStateDtos());
        given(geoLocationService.getAllCountries()).willReturn(Lists.newArrayList(Country.builder().name("Atlantis").build()));

        airVisualService.refreshStates();

        then(geoLocationService).should(times(1)).getAllCountries();
        then(airVisualClient).should(times(1)).getAllStatesForCountry(eq("Atlantis"));
        then(pollingConfiguration).should(times(1)).getTimeBetweenRequests();
        then(geoLocationService).should().upsertStates(statesArgumentCaptor.capture());
        then(airVisualClient).shouldHaveNoMoreInteractions();
        then(geoLocationService).shouldHaveNoMoreInteractions();
        then(pollingConfiguration).shouldHaveNoMoreInteractions();
        assertThat(statesArgumentCaptor.getValue()).isEqualTo(Lists.newArrayList(
                State.builder().name("Some State In Atlantis").build()
        ));
    }

    @Test
    public void shouldNotRefreshStates() {
        given(geoLocationService.getAllCountries()).willReturn(Lists.emptyList());

        airVisualService.refreshStates();

        then(geoLocationService).should(times(1)).getAllCountries();
        then(geoLocationService).shouldHaveNoMoreInteractions();
        then(airVisualClient).shouldHaveZeroInteractions();
        then(pollingConfiguration).shouldHaveZeroInteractions();
    }

    @Test
    public void shouldRefreshCities() {
        given(airVisualClient.getAllCitiesForStateAndCountry("Some State In Atlantis", "Atlantis")).willReturn(buildCityDtos());
        given(geoLocationService.getAllStates()).willReturn(Lists.newArrayList(State.builder().countryName("Atlantis").name("Some State In Atlantis").build()));

        airVisualService.refreshCities();

        then(geoLocationService).should(times(1)).getAllStates();
        then(airVisualClient).should(times(1)).getAllCitiesForStateAndCountry(eq("Some State In Atlantis"), eq("Atlantis"));
        then(pollingConfiguration).should(times(1)).getTimeBetweenRequests();
        then(geoLocationService).should(times(1)).upsertCities(citiesArgumentCaptor.capture());
        then(airVisualClient).shouldHaveNoMoreInteractions();
        then(geoLocationService).shouldHaveNoMoreInteractions();
        then(pollingConfiguration).shouldHaveNoMoreInteractions();
        assertThat(citiesArgumentCaptor.getValue()).isEqualTo(Lists.newArrayList(
                City.builder().name("Some City In Some State In Atlantis").build()
        ));
    }
}
