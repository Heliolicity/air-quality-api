package com.heliolicity.airqualityapi.service.impl;

import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;
import com.heliolicity.airqualityapi.exception.InsertException;
import com.heliolicity.airqualityapi.exception.UpdateException;
import com.heliolicity.airqualityapi.mapper.GeoLocationMapper;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.heliolicity.airqualityapi.util.TestUtil.buildCities;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCitiesForCountry;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCitiesForState;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCitiesToUpsert;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountries;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountriesToUpsert;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStates;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStatesForCountry;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStatesToUpsert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@RunWith(MockitoJUnitRunner.class)
public class GeoLocationServiceImplTest {

    @InjectMocks
    private GeoLocationServiceImpl geoLocationService;

    @Mock
    private GeoLocationMapper geoLocationMapper;

    @Test
    public void shouldGetAllCountries() {
        given(geoLocationMapper.findAllCountries()).willReturn(buildCountries());

        List<Country> countries = geoLocationService.getAllCountries();

        assertThat(countries).isEqualTo(buildCountries());
    }

    @Test
    public void shouldNotGetAllCountries() {
        given(geoLocationMapper.findAllCountries()).willReturn(Lists.emptyList());

        List<Country> countries = geoLocationService.getAllCountries();

        assertThat(countries).isEmpty();
    }

    @Test
    public void shouldGetAllStates() {
        given(geoLocationMapper.findAllStates()).willReturn(buildStates());

        List<State> states = geoLocationService.getAllStates();

        assertThat(states).isEqualTo(buildStates());
    }


    @Test
    public void shouldNotGetAllStates() {
        given(geoLocationMapper.findAllStates()).willReturn(Lists.emptyList());

        List<State> states = geoLocationService.getAllStates();

        assertThat(states).isEmpty();
    }

    @Test
    public void shouldGetAllStatesForCountry() {
        given(geoLocationMapper.findAllCountries()).willReturn(Lists.newArrayList(Country.builder().name("Atlantis").build()));
        given(geoLocationMapper.findAllStatesForCountry(Country.builder().name("Atlantis").build())).willReturn(buildStatesForCountry());

        List<State> states = geoLocationService.getAllStatesForCountry(Country.builder().name("Atlantis").build());

        assertThat(states).isEqualTo(buildStatesForCountry());
    }

    @Test
    public void shouldNotGetAllStatesForCountry() {
        List<State> states = geoLocationService.getAllStatesForCountry(Country.builder().name("SOME COUNTRY THAT DOES NOT EXIST").build());

        assertThat(states).isEmpty();
    }

    @Test
    public void shouldNotGetAllStatesForCountryWhenCountryNameIsInvalid() {
        given(geoLocationMapper.findAllCountries()).willReturn(Lists.newArrayList(Country.builder().name("THIS COUNTRY").build()));

        List<State> states = geoLocationService.getAllStatesForCountry(Country.builder().name("ANOTHER COUNTRY").build());

        then(geoLocationMapper).should().findAllCountries();
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
        assertThat(states).isEmpty();
    }

    @Test
    public void shouldGetAllCities() {
        given(geoLocationMapper.findAllCities()).willReturn(buildCities());

        List<City> cities = geoLocationService.getAllCities();

        assertThat(cities).isEqualTo(buildCities());
    }

    @Test
    public void shouldNotGetAllCities() {
        given(geoLocationMapper.findAllCities()).willReturn(Lists.emptyList());

        List<City> cities = geoLocationService.getAllCities();

        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldGetAllCitiesForState() {
        given(geoLocationMapper.findAllCountries()).willReturn(Lists.newArrayList(Country.builder().name("Atlantis").build()));
        given(geoLocationMapper.findAllStates()).willReturn(Lists.newArrayList(State.builder().name("Some State In Atlantis").build()));
        given(geoLocationMapper.findAllCitiesForState(State.builder().countryName("Atlantis").name("Some State In Atlantis").build())).willReturn(buildCitiesForState());

        List<City> cities = geoLocationService.getAllCitiesForState(State.builder().countryName("Atlantis").name("Some State In Atlantis").build());

        assertThat(cities).isEqualTo(buildCitiesForState());
    }

    @Test
    public void shouldNotGetAllCitiesForStateWhenCountryNameIsInvalid() {
        given(geoLocationMapper.findAllCountries()).willReturn(Lists.newArrayList(Country.builder().name("THIS COUNTRY").build()));
        given(geoLocationMapper.findAllStates()).willReturn(Lists.newArrayList(State.builder().name("THIS STATE").build()));

        List<City> cities = geoLocationService.getAllCitiesForState(State.builder().name("THIS STATE").countryName("ANOTHER COUNTRY").build());

        then(geoLocationMapper).should().findAllCountries();
        then(geoLocationMapper).should().findAllStates();
        then(geoLocationMapper).shouldHaveZeroInteractions();
        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldNotGetAllCitiesForStateWhenStateNameIsInvalid() {
        given(geoLocationMapper.findAllStates()).willReturn(Lists.newArrayList(State.builder().name("THIS STATE").build()));

        List<City> cities = geoLocationService.getAllCitiesForState(State.builder().name("ANOTHER STATE").countryName("THIS COUNTRY").build());

        then(geoLocationMapper).should().findAllStates();
        then(geoLocationMapper).shouldHaveZeroInteractions();
        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldGetAllCitiesForCountry() {
        given(geoLocationMapper.findAllCountries()).willReturn(Lists.newArrayList(Country.builder().name("Val Verde").build()));
        given(geoLocationMapper.findAllCitiesForCountry(Country.builder().name("Val Verde").build())).willReturn(buildCitiesForCountry());

        List<City> cities = geoLocationService.getAllCitiesForCountry(Country.builder().name("Val Verde").build());

        assertThat(cities).isEqualTo(buildCitiesForCountry());
    }

    @Test
    public void shouldNotGetAllCitiesForCountry() {
        given(geoLocationMapper.findAllCountries()).willReturn(Lists.newArrayList(Country.builder().name("THIS COUNTRY").build()));

        List<City> cities = geoLocationService.getAllCitiesForCountry(Country.builder().name("ANOTHER COUNTRY").build());

        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldUpsertCountriesThatDoNotAlreadyExist() {
        given(geoLocationMapper.findCountryByName("Northern Ireland")).willReturn(Optional.empty());
        given(geoLocationMapper.findCountryByName("Narnia")).willReturn(Optional.empty());
        given(geoLocationMapper.insertCountry(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.insertCountry(Country.builder().id(BigInteger.valueOf(4)).name("Narnia").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertCountries(buildCountriesToUpsert());

        then(geoLocationMapper).should().findCountryByName(eq("Northern Ireland"));
        then(geoLocationMapper).should().insertCountry(eq(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findCountryByName(eq("Narnia"));
        then(geoLocationMapper).should().insertCountry(eq(Country.builder().id(BigInteger.valueOf(4)).name("Narnia").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldUpsertCountriesThatAlreadyExist() {
        given(geoLocationMapper.findCountryByName("Northern Ireland")).willReturn(Optional.of(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.findCountryByName("Narnia")).willReturn(Optional.of(Country.builder().id(BigInteger.valueOf(4)).name("Narnia").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.updateCountry(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.updateCountry(Country.builder().id(BigInteger.valueOf(4)).name("Narnia").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertCountries(buildCountriesToUpsert());

        then(geoLocationMapper).should().findCountryByName(eq("Northern Ireland"));
        then(geoLocationMapper).should().updateCountry(eq(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findCountryByName(eq("Narnia"));
        then(geoLocationMapper).should().updateCountry(eq(Country.builder().id(BigInteger.valueOf(4)).name("Narnia").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldUpsertCountriesWhereOneAlreadyExists() {
        given(geoLocationMapper.findCountryByName("Northern Ireland")).willReturn(Optional.of(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.findCountryByName("Narnia")).willReturn(Optional.empty());
        given(geoLocationMapper.updateCountry(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.insertCountry(Country.builder().id(BigInteger.valueOf(4)).name("Narnia").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertCountries(buildCountriesToUpsert());

        then(geoLocationMapper).should().findCountryByName(eq("Northern Ireland"));
        then(geoLocationMapper).should().updateCountry(eq(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findCountryByName(eq("Narnia"));
        then(geoLocationMapper).should().insertCountry(eq(Country.builder().id(BigInteger.valueOf(4)).name("Narnia").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldNotUpsertCountries() {
        geoLocationService.upsertCountries(Lists.emptyList());

        then(geoLocationMapper).shouldHaveZeroInteractions();
    }

    @Test
    public void shouldThrowInsertExceptionWhenUpsertingCountries() {
        given(geoLocationMapper.findCountryByName("Northern Ireland")).willReturn(Optional.empty());

        Throwable caughtException = catchThrowable(() -> geoLocationService.upsertCountries(Lists.newArrayList(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())));

        assertThat(caughtException).isInstanceOf(InsertException.class);
    }

    @Test
    public void shouldThrowUpdateExceptionWhenUpsertingCountries() {
        given(geoLocationMapper.findCountryByName("Northern Ireland")).willReturn(Optional.of(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));

        Throwable caughtException = catchThrowable(() -> geoLocationService.upsertCountries(Lists.newArrayList(Country.builder().id(BigInteger.valueOf(3)).name("Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())));

        assertThat(caughtException).isInstanceOf(UpdateException.class);
    }

    @Test
    public void shouldUpsertStatesThatDoNotAlreadyExist() {
        given(geoLocationMapper.findStateByName("Londonderry")).willReturn(Optional.empty());
        given(geoLocationMapper.findStateByName("Harmony")).willReturn(Optional.empty());
        given(geoLocationMapper.insertState(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.insertState(State.builder().id(BigInteger.valueOf(4)).countryName("Shangri-La").name("Harmony").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertStates(buildStatesToUpsert());

        then(geoLocationMapper).should().findStateByName(eq("Londonderry"));
        then(geoLocationMapper).should().insertState(eq(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findStateByName(eq("Harmony"));
        then(geoLocationMapper).should().insertState(eq(State.builder().id(BigInteger.valueOf(4)).countryName("Shangri-La").name("Harmony").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldUpsertStatesThatAlreadyExist() {
        given(geoLocationMapper.findStateByName("Londonderry")).willReturn(Optional.of(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.findStateByName("Harmony")).willReturn(Optional.of(State.builder().id(BigInteger.valueOf(4)).countryName("Shangri-La").name("Harmony").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.updateState(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.updateState(State.builder().id(BigInteger.valueOf(4)).countryName("Shangri-La").name("Harmony").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertStates(buildStatesToUpsert());

        then(geoLocationMapper).should().findStateByName(eq("Londonderry"));
        then(geoLocationMapper).should().updateState(eq(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findStateByName(eq("Harmony"));
        then(geoLocationMapper).should().updateState(eq(State.builder().id(BigInteger.valueOf(4)).countryName("Shangri-La").name("Harmony").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldUpsertStatesWhereOneAlreadyExists() {
        given(geoLocationMapper.findStateByName("Londonderry")).willReturn(Optional.of(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.findStateByName("Harmony")).willReturn(Optional.empty());
        given(geoLocationMapper.updateState(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.insertState(State.builder().id(BigInteger.valueOf(4)).countryName("Shangri-La").name("Harmony").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertStates(buildStatesToUpsert());

        then(geoLocationMapper).should().findStateByName(eq("Londonderry"));
        then(geoLocationMapper).should().updateState(eq(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findStateByName(eq("Harmony"));
        then(geoLocationMapper).should().insertState(eq(State.builder().id(BigInteger.valueOf(4)).countryName("Shangri-La").name("Harmony").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldNotUpsertStates() {
        geoLocationService.upsertStates(Lists.emptyList());

        then(geoLocationMapper).shouldHaveZeroInteractions();
    }

    @Test
    public void shouldThrowInsertExceptionWhenUpsertingStates() {
        given(geoLocationMapper.findStateByName("Londonderry")).willReturn(Optional.empty());

        Throwable caughtException = catchThrowable(() -> geoLocationService.upsertStates(Lists.newArrayList(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())));

        assertThat(caughtException).isInstanceOf(InsertException.class);
    }

    @Test
    public void shouldThrowUpdateExceptionWhenUpsertingStates() {
        given(geoLocationMapper.findStateByName("Londonderry")).willReturn(Optional.of(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));

        Throwable caughtException = catchThrowable(() -> geoLocationService.upsertStates(Lists.newArrayList(State.builder().id(BigInteger.valueOf(3)).countryName("Northern Ireland").name("Londonderry").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())));

        assertThat(caughtException).isInstanceOf(UpdateException.class);
    }

    @Test
    public void shouldUpsertCitiesThatDoNotAlreadyExist() {
        given(geoLocationMapper.findCityByName("Some City In Londonderry In Northern Ireland")).willReturn(Optional.empty());
        given(geoLocationMapper.findCityByName("Some City In Harmony In Shangri-La")).willReturn(Optional.empty());
        given(geoLocationMapper.insertCity(City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.insertCity(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertCities(buildCitiesToUpsert());

        then(geoLocationMapper).should().findCityByName(eq("Some City In Londonderry In Northern Ireland"));
        then(geoLocationMapper).should().insertCity(eq(City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findCityByName(eq("Some City In Harmony In Shangri-La"));
        then(geoLocationMapper).should().insertCity(eq(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldUpsertCitiesThatAlreadyExist() {
        given(geoLocationMapper.findCityByName("Some City In Londonderry In Northern Ireland")).willReturn(Optional.of(City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.findCityByName("Some City In Harmony In Shangri-La")).willReturn(Optional.of(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.updateCity(City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.updateCity(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertCities(buildCitiesToUpsert());

        then(geoLocationMapper).should().findCityByName(eq("Some City In Londonderry In Northern Ireland"));
        then(geoLocationMapper).should().updateCity(eq(City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findCityByName(eq("Some City In Harmony In Shangri-La"));
        then(geoLocationMapper).should().updateCity(eq(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldUpsertCitiesWhereOneAlreadyExists() {
        given(geoLocationMapper.findCityByName("Some City In Londonderry In Northern Ireland")).willReturn(Optional.of(City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        given(geoLocationMapper.findCityByName("Some City In Harmony In Shangri-La")).willReturn(Optional.empty());
        given(geoLocationMapper.updateCity(City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);
        given(geoLocationMapper.insertCity(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())).willReturn(1);

        geoLocationService.upsertCities(buildCitiesToUpsert());

        then(geoLocationMapper).should().findCityByName(eq("Some City In Londonderry In Northern Ireland"));
        then(geoLocationMapper).should().updateCity(eq(City.builder().id(BigInteger.valueOf(5)).stateId(BigInteger.valueOf(3)).stateName("Some State In Val Verde").name("Some City In Londonderry In Northern Ireland").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).should().findCityByName(eq("Some City In Harmony In Shangri-La"));
        then(geoLocationMapper).should().insertCity(eq(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));
        then(geoLocationMapper).shouldHaveNoMoreInteractions();
    }

    @Test
    public void shouldNotUpsertCities() {
        geoLocationService.upsertCities(Lists.emptyList());

        then(geoLocationMapper).shouldHaveZeroInteractions();
    }

    @Test
    public void shouldThrowInsertExceptionWhenUpsertingCities() {
        given(geoLocationMapper.findCityByName("Some City In Harmony In Shangri-La")).willReturn(Optional.empty());

        Throwable caughtException = catchThrowable(() -> geoLocationService.upsertCities(Lists.newArrayList(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())));

        assertThat(caughtException).isInstanceOf(InsertException.class);
    }

    @Test
    public void shouldThrowUpdateExceptionWhenUpsertingCities() {
        given(geoLocationMapper.findCityByName("Some City In Harmony In Shangri-La")).willReturn(Optional.of(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build()));

        Throwable caughtException = catchThrowable(() -> geoLocationService.upsertCities(Lists.newArrayList(City.builder().id(BigInteger.valueOf(6)).stateId(BigInteger.valueOf(4)).stateName("Harmony").name("Some City In Harmony In Shangri-La").lastModified(LocalDateTime.of(2019, 6, 30, 0, 0)).build())));

        assertThat(caughtException).isInstanceOf(UpdateException.class);
    }
}
