package com.heliolicity.airqualityapi.client.impl;

import com.heliolicity.airqualityapi.client.dto.CitiesList;
import com.heliolicity.airqualityapi.client.dto.CityDto;
import com.heliolicity.airqualityapi.client.dto.CountriesList;
import com.heliolicity.airqualityapi.client.dto.CountryDto;
import com.heliolicity.airqualityapi.client.dto.QueryParam;
import com.heliolicity.airqualityapi.client.dto.StateDto;
import com.heliolicity.airqualityapi.client.dto.StatesList;
import com.heliolicity.airqualityapi.configuration.AirVisualConfiguration;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;

import static com.heliolicity.airqualityapi.util.TestUtil.buildCitiesList;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCityDtos;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountriesList;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountryDtos;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStateDtos;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStatesList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.in;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class AirVisualClientImplTest {

    @InjectMocks
    private AirVisualClientImpl airVisualClient;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private AirVisualConfiguration airVisualConfiguration;

    @Before
    public void setUp() {
        given(airVisualConfiguration.getApiKey()).willReturn("API_KEY");
        given(airVisualConfiguration.getCountriesApi()).willReturn("/countriesendpoint");
        given(airVisualConfiguration.getStatesApi()).willReturn("/statesendpoint");
        given(airVisualConfiguration.getCitiesApi()).willReturn("/citiesendpoint");
    }

    @Test
    public void shouldGetAllCountries() {
        URI uri = buildURI(airVisualConfiguration.getCountriesApi(), airVisualConfiguration.getApiKey(), Collections.emptyList());
        given(restTemplate.getForObject(uri, CountriesList.class)).willReturn(buildCountriesList("success", buildCountryDtos()));

        List<CountryDto> countries = airVisualClient.getAllCountries();

        assertThat(countries).isEqualTo(buildCountryDtos());
    }

    @Test
    public void shouldNotGetAllCountriesWhenUriContainsIllegalCharacter() {
        given(airVisualConfiguration.getApiKey()).willReturn("   ");

        List<CountryDto> countries = airVisualClient.getAllCountries();

        assertThat(countries).isEmpty();
    }

    @Test
    public void shouldNotGetAllCountriesWhenRestTemplateReturnsNull() {
        URI uri = buildURI(airVisualConfiguration.getCountriesApi(), airVisualConfiguration.getApiKey(), Collections.emptyList());
        given(restTemplate.getForObject(uri, CountriesList.class)).willReturn(null);

        List<CountryDto> countries = airVisualClient.getAllCountries();

        assertThat(countries).isEmpty();
    }

    @Test
    public void shouldNotGetAllCountriesWhenRestTemplateCountriesListResponseIsNotSuccessful() {
        URI uri = buildURI(airVisualConfiguration.getCountriesApi(), airVisualConfiguration.getApiKey(), Collections.emptyList());
        given(restTemplate.getForObject(uri, CountriesList.class)).willReturn(buildCountriesList("failure", Lists.emptyList()));

        List<CountryDto> countries = airVisualClient.getAllCountries();

        assertThat(countries).isEmpty();
    }

    @Test
    public void shouldNotGetAllCountriesWhenRestTemplateCountriesListResponseContainsNullList() {
        URI uri = buildURI(airVisualConfiguration.getCountriesApi(), airVisualConfiguration.getApiKey(), Collections.emptyList());
        given(restTemplate.getForObject(uri, CountriesList.class)).willReturn(buildCountriesList("success", null));

        List<CountryDto> countries = airVisualClient.getAllCountries();

        assertThat(countries).isEmpty();
    }

    @Test
    public void shouldThrowExceptionWhenConnectingToCountriesEndpoint() {
        URI uri = buildURI(airVisualConfiguration.getCountriesApi(), airVisualConfiguration.getApiKey(), Collections.emptyList());
        given(restTemplate.getForObject(uri, CountriesList.class)).willAnswer(invocationOnMock -> { throw new Exception(); });

        List<CountryDto> countries = airVisualClient.getAllCountries();

        assertThat(countries).isEmpty();
    }

    @Test
    public void shouldGetAllStatesForCountry() {
        URI uri = buildURI(airVisualConfiguration.getStatesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, StatesList.class)).willReturn(buildStatesList("success", buildStateDtos()));

        List<StateDto> states = airVisualClient.getAllStatesForCountry("Atlantis");

        assertThat(states).isEqualTo(buildStateDtos());
    }

    @Test
    public void shouldNotGetAllStatesForCountryWhenUriContainsIllegalCharacter() {
        given(airVisualConfiguration.getApiKey()).willReturn("   ");

        List<StateDto> states = airVisualClient.getAllStatesForCountry("Atlantis");

        assertThat(states).isEmpty();
    }

    @Test
    public void shouldNotGetAllStatesForCountryWhenRestTemplateReturnsNull() {
        URI uri = buildURI(airVisualConfiguration.getStatesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, StatesList.class)).willReturn(null);

        List<StateDto> states = airVisualClient.getAllStatesForCountry("Atlantis");

        assertThat(states).isEmpty();
    }

    @Test
    public void shouldNotGetAllStatesForCountryWhenRestTemplateStatesListResponseIsNotSuccessful() {
        URI uri = buildURI(airVisualConfiguration.getStatesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, StatesList.class)).willReturn(buildStatesList("failure", Lists.emptyList()));

        List<StateDto> states = airVisualClient.getAllStatesForCountry("Atlantis");

        assertThat(states).isEmpty();
    }

    @Test
    public void shouldNotGetAllStatesForCountryWhenRestTemplateStatesListContainsNullList() {
        URI uri = buildURI(airVisualConfiguration.getStatesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, StatesList.class)).willReturn(buildStatesList("failure", null));

        List<StateDto> states = airVisualClient.getAllStatesForCountry("Atlantis");

        assertThat(states).isEmpty();
    }

    @Test
    public void shouldThrowExceptionWhenConnectingToStatesEndpoint() {
        URI uri = buildURI(airVisualConfiguration.getStatesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, StatesList.class)).willAnswer(invocationOnMock -> { throw new Exception(); });

        List<StateDto> states = airVisualClient.getAllStatesForCountry("Atlantis");

        assertThat(states).isEmpty();
    }

    @Test
    public void shouldGetAllCitiesForStateAndCountry() {
        URI uri = buildURI(airVisualConfiguration.getCitiesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("state").value("Some State In Atlantis").build(),
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, CitiesList.class)).willReturn(buildCitiesList("success", buildCityDtos()));

        List<CityDto> cities = airVisualClient.getAllCitiesForStateAndCountry("Some State In Atlantis", "Atlantis");

        assertThat(cities).isEqualTo(buildCityDtos());
    }

    @Test
    public void shouldNotGetAllCitiesForStateAndCountryWhenUriContainsIllegalCharacter() {
        given(airVisualConfiguration.getApiKey()).willReturn("   ");

        List<CityDto> cities = airVisualClient.getAllCitiesForStateAndCountry("Some State In Atlantis", "Atlantis");

        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldNotGetAllCitiesForStateAndCountryWhenRestTemplateReturnsNull() {
        URI uri = buildURI(airVisualConfiguration.getCitiesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("state").value("Some State In Atlantis").build(),
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, CitiesList.class)).willReturn(null);

        List<CityDto> cities = airVisualClient.getAllCitiesForStateAndCountry("Some State In Atlantis", "Atlantis");

        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldNotGetAllCitiesForStateAndCountryWhenRestTemplateCitiesListResponseIsNotSuccessful() {
        URI uri = buildURI(airVisualConfiguration.getCitiesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("state").value("Some State In Atlantis").build(),
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, CitiesList.class)).willReturn(buildCitiesList("failure", Lists.emptyList()));

        List<CityDto> cities = airVisualClient.getAllCitiesForStateAndCountry("Some State In Atlantis", "Atlantis");

        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldNotGetAllCitiesForStateAndCountryWhenRestTemplateCitiesListResponseContainsNullList() {
        URI uri = buildURI(airVisualConfiguration.getCitiesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("state").value("Some State In Atlantis").build(),
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, CitiesList.class)).willReturn(buildCitiesList("success", null));

        List<CityDto> cities = airVisualClient.getAllCitiesForStateAndCountry("Some State In Atlantis", "Atlantis");

        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldThrowExceptionWhenConnectingToCitiesEndpoint() {
        URI uri = buildURI(airVisualConfiguration.getCitiesApi(), airVisualConfiguration.getApiKey(), Lists.newArrayList(
                QueryParam.builder().key("state").value("Some State In Atlantis").build(),
                QueryParam.builder().key("country").value("Atlantis").build()
        ));
        given(restTemplate.getForObject(uri, CitiesList.class)).willAnswer(invocationOnMock -> { throw new Exception(); });

        List<CityDto> cities = airVisualClient.getAllCitiesForStateAndCountry("Some State In Atlantis", "Atlantis");

        assertThat(cities).isEmpty();
    }

    private URI buildURI(String api, String key, List<QueryParam> queryParams) {
        try {
            if (queryParams.isEmpty()) {
                return new URI(api + "?key=" + key);
            }
            StringBuilder uriBuilder = new StringBuilder(api + "?key=" + key + "&");

            for (QueryParam queryParam : queryParams) {
                uriBuilder.append(queryParam.getKey())
                        .append("=")
                        .append(URLEncoder.encode(queryParam.getValue(), "UTF-8"))
                        .append("&");
            }

            String uri = uriBuilder.toString();

            if (uri.endsWith("&")) {
                uri = uri.substring(0, uri.length() - 1);
            }
            return new URI(uri);
        }
        catch (URISyntaxException | UnsupportedEncodingException use) {
            return null;
        }
    }
}
