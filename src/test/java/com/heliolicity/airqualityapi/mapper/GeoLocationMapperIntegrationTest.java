package com.heliolicity.airqualityapi.mapper;

import com.heliolicity.airqualityapi.DemoApplication;
import com.heliolicity.airqualityapi.domain.City;
import com.heliolicity.airqualityapi.domain.Country;
import com.heliolicity.airqualityapi.domain.State;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import static com.heliolicity.airqualityapi.util.TestUtil.buildCities;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCitiesForCountry;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCitiesForState;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountries;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountriesToUpsert;
import static com.heliolicity.airqualityapi.util.TestUtil.buildCountry;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStates;
import static com.heliolicity.airqualityapi.util.TestUtil.buildStatesForCountry;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@MybatisTest
@Sql(scripts = {"classpath:db/1_CREATE_SCHEMA.sql", "classpath:db/2_INSERT_DATA.sql"})
public class GeoLocationMapperIntegrationTest {

    @Autowired
    private GeoLocationMapper geoLocationMapper;

    @Test
    public void smokeTest() {

    }

    @Test
    public void shouldFindAllCountries() {
        List<Country> countries = geoLocationMapper.findAllCountries();

        assertThat(countries.size()).isEqualTo(2);
        assertThat(countries).isEqualTo(buildCountries());
    }

    @Test
    public void shouldFindCountryByName() {
        Optional<Country> optionalCountry = geoLocationMapper.findCountryByName("Atlantis");

        assertThat(optionalCountry.isPresent()).isTrue();
        assertThat(optionalCountry.get()).isEqualToComparingFieldByField(buildCountry("Atlantis"));
    }

    @Test
    public void shouldFindAllStates() {
        List<State> states = geoLocationMapper.findAllStates();

        assertThat(states.size()).isEqualTo(4);
        assertThat(states).isEqualTo(buildStates());
    }

    @Test
    public void shouldFindAllStatesForCountry() {
        List<State> states = geoLocationMapper.findAllStatesForCountry(Country.builder().name("Atlantis").build());

        assertThat(states.size()).isEqualTo(2);
        assertThat(states).isEqualTo(buildStatesForCountry());
    }

    @Test
    public void shouldNotFindAllStatesForCountry() {
        List<State> states = geoLocationMapper.findAllStatesForCountry(Country.builder().name("SOME COUNTRY THAT DOES NOT EXIST").build());

        assertThat(states).isEmpty();
    }

    @Test
    public void shouldFindStateByName() {
        Optional<State> optionalState = geoLocationMapper.findStateByName("Some State In Atlantis");

        assertThat(optionalState.isPresent()).isTrue();
        assertThat(optionalState.get().getCountryName()).isEqualToIgnoringCase("Atlantis");
        assertThat(optionalState.get().getName()).isEqualToIgnoringCase("Some State In Atlantis");
    }

    @Test
    public void shouldFindAllCities() {
        List<City> cities = geoLocationMapper.findAllCities();

        assertThat(cities.size()).isEqualTo(4);
        assertThat(cities).isEqualTo(buildCities());
    }

    @Test
    public void shouldFindAllCitiesForState() {
        List<City> cities = geoLocationMapper.findAllCitiesForState(State.builder().name("Some State In Atlantis").build());

        assertThat(cities.size()).isEqualTo(2);
        assertThat(cities).isEqualTo(buildCitiesForState());
    }

    @Test
    public void shouldNotFindAllCitiesForState() {
        List<City> cities = geoLocationMapper.findAllCitiesForState(State.builder().name("SOME STATE THAT DOES NOT EXIST").build());

        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldFindAllCitiesForCountry() {
        List<City> cities = geoLocationMapper.findAllCitiesForCountry(Country.builder().name("Val Verde").build());

        assertThat(cities.size()).isEqualTo(1);
        assertThat(cities).isEqualTo(buildCitiesForCountry());
    }

    @Test
    public void shouldNotFindAllCitiesForCountry() {
        List<City> cities = geoLocationMapper.findAllCitiesForCountry(Country.builder().name("SOME COUNTRY THAT DOES NOT EXIST").build());

        assertThat(cities).isEmpty();
    }

    @Test
    public void shouldFindCityByName() {
        Optional<City> optionalCity = geoLocationMapper.findCityByName("Some City In Some State In Atlantis");

        assertThat(optionalCity.isPresent()).isTrue();
        assertThat(optionalCity.get().getStateName()).isEqualToIgnoringCase("Some State In Atlantis");
        assertThat(optionalCity.get().getName()).isEqualToIgnoringCase("Some City In Some State In Atlantis");
    }

    @Test
    public void shouldInsertCountry() {
        Country country = Country.builder().name("Northern Ireland").build();

        Integer result = geoLocationMapper.insertCountry(country);

        assertThat(result).isEqualTo(1);
        Country insertedCountry = geoLocationMapper.findCountryByName("Northern Ireland").get();
        assertThat(insertedCountry.getName()).isEqualToIgnoringCase("Northern Ireland");
    }

    @Test
    public void shouldUpdateCountry() {
        Country country = geoLocationMapper.findCountryByName("Atlantis").get();
        country.setName("New Atlantis");

        Integer result = geoLocationMapper.updateCountry(country);

        assertThat(result).isEqualTo(1);
        Country updatedCountry = geoLocationMapper.findCountryByName("New Atlantis").get();
        assertThat(updatedCountry.getName()).isEqualToIgnoringCase("New Atlantis");
    }

    @Ignore
    @Test
    public void shouldUpsertCountries() {
        List<Country> countries = buildCountriesToUpsert();

        Integer result = geoLocationMapper.upsertCountries(countries);

        assertThat(result).isEqualTo(1);
    }

    @Test
    public void shouldInsertState() {
        State state = State.builder().countryId(BigInteger.valueOf(2)).name("Some State").build();

        Integer result = geoLocationMapper.insertState(state);

        assertThat(result).isEqualTo(1);
        State insertedState = geoLocationMapper.findStateByName("Some State").get();
        assertThat(insertedState.getCountryName()).isEqualToIgnoringCase("Val Verde");
        assertThat(insertedState.getName()).isEqualToIgnoringCase("Some State");
    }

    @Test
    public void shouldUpdateSate() {
        State state = geoLocationMapper.findStateByName("Some State In Atlantis").get();
        state.setName("New State Name");

        Integer result = geoLocationMapper.updateState(state);

        assertThat(result).isEqualTo(1);
        State updatedState = geoLocationMapper.findStateByName("New State Name").get();
        assertThat(updatedState.getName()).isEqualToIgnoringCase("New State Name");
    }

    @Ignore
    @Test
    public void shouldUpsertStates() {

    }

    @Test
    public void shouldInsertCity() {
        City city = City.builder().stateId(BigInteger.valueOf(2)).name("Some City").build();

        Integer result = geoLocationMapper.insertCity(city);

        assertThat(result).isEqualTo(1);
        City insertedCity = geoLocationMapper.findCityByName("Some City").get();
        assertThat(insertedCity.getStateName()).isEqualToIgnoringCase("Some Other State In Atlantis");
        assertThat(insertedCity.getName()).isEqualToIgnoringCase("Some City");
    }

    @Test
    public void shouldUpdateCity() {
        City city = geoLocationMapper.findCityByName("Some City In Some State In Atlantis").get();
        city.setName("New City Name");

        Integer result = geoLocationMapper.updateCity(city);

        assertThat(result).isEqualTo(1);
        City updatedCity = geoLocationMapper.findCityByName("New City Name").get();
        assertThat(updatedCity.getName()).isEqualToIgnoringCase("New City Name");
    }

    @Ignore
    @Test
    public void shouldUpsertCities() {

    }
}
